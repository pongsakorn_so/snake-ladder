# Snake and Ladder Refactor #
This is a snake and ladder game that refactored form the bad design snake and ladder using the GRASP Principle.

### What is change? ###
I split the code from the mainActivity into various part. 

I split the player position into
the player class and then remove the responsibility of random a number that use as a dice value and create Dice instead. 

Create a abstract class square then creating new type of square wil easier,just extends the square object. I create normalSquare,boostSquare,goalSquare. Using abstract I can treat all of these type of squares as square. Then I keep the square in the board because board will own all of the square.

I create a game object to be a controller. Game will have board,player,dice. Gameactivity will only communicate with the game object.

In boardView I decide to own a board. So when draw a square boardView can get the square of that position and then get text color and cell color from it
