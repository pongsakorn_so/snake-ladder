package com.ske.snakebaddesign.domain;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by Asus on 3/15/2016.
 */
public class Player {
    private String name;
    private int color;
    private int position;

    public Player(String name){
        this.name = name;
        this.position = 0;
        this.color = Color.BLACK;
    }

    public Player(String name , int color){
        this.name = name;
        this.position= 0;
        this.color = color;
    }

    public String getName(){
        return name;
    }

    public int getPosition(){
        return position;
    }

    public void setPosition(int position){
        this.position = position;
    }

    public void rollDice(Dice dice){
       dice.roll();
    }

    public int getColor(){
        return this.color;
    }


}
