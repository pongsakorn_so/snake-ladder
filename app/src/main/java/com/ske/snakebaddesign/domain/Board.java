package com.ske.snakebaddesign.domain;

import java.util.Random;

/**
 * Created by Asus on 3/15/2016.
 */
public class Board {
    private int boardSize;
    private Square[] squares;

    public Board(int boardSize){
        this.boardSize = boardSize;
        squares = new Square[boardSize * boardSize];
        fillBoard();
    }

    public void fillBoard(){
        squares[0] = new StartSquare();
        for(int i=1;i<boardSize*boardSize ;i++){
            int random = new Random().nextInt(2);

            if(random == 1)
                squares[i] = new BoostSquare();
            else
                squares[i] = new NormalSquare((i+1)+ "");
        }
        squares[boardSize*boardSize-1] = new GoalSquare();
    }

    public int getBoardSize(){
        return boardSize;
    }

    public Square[] getSquares(){
        return  squares;
    }


}
