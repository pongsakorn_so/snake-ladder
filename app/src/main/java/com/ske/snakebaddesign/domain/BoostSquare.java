package com.ske.snakebaddesign.domain;

import android.content.DialogInterface;
import android.graphics.Color;

import com.ske.snakebaddesign.activities.GameActivity;

import java.util.Random;

/**
 * Created by Asus on 3/17/2016.
 */
public class BoostSquare extends Square {

    private final String label = "BOOST";
    private final int squareColor = Color.RED;
    private final int textColor = Color.WHITE;

    public BoostSquare(){
        super();
        setLabel(label);
        setSquareColor(squareColor);
        setTextColor(textColor);
    }

    @Override
    public GameAction getGameAction() {
        GameAction boost = new GameAction() {
            @Override
            public void doAction(final GameActivity gameActivity) {

                String title = "BOOST EFFECT";
                int boardSize = gameActivity.getGame().getBoard().getBoardSize();
                final int random = 1 + new Random().nextInt(6);
                String msg ="Boost "+ random+" Square ";

                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        gameActivity.moveCurrentPiece(random);
                        dialog.dismiss();
                    }
                };
                gameActivity.displayDialog(title, msg, listener);

            }
        };
        return  boost;
    }
}
