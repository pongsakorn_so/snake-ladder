package com.ske.snakebaddesign.domain;

import android.content.DialogInterface;
import android.graphics.Color;

import java.util.Observable;

/**
 * Created by Asus on 3/16/2016.
 */
public class Game extends Observable{
    private Board board;
    private Dice dice;
    //private Player p1,p2;
    private Player[] players;
    private int turn;

    public Game(){
        board = new Board(6);
        dice = new Dice();

//        p1 = new Player("Player1" , Color.WHITE);
//        p2 = new Player("Player2" , Color.BLACK);

        players = new Player[2];
        players[0] = new Player("Player1" , Color.WHITE);
        players[1] = new Player("Player2" , Color.BLACK);

        turn = 0;
    }

    public int getTurn(){
        return turn;
    }

    public void setTurn(int turn){
        this.turn = turn;
    }

    public Board getBoard(){
        return board;
    }

    public Dice getDice(){
        return dice;
    }

    public void resetGame() {
        turn = 0;
        for(Player player : players)
            player.setPosition(0);

        notifyObservers();
        setChanged();
    }

    public int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = board.getBoardSize() * board.getBoardSize() - 1;
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }

    public Player getCurrentPlayer(){
        return players[turn%2];
    }

    public int rollDice(){
        getCurrentPlayer().rollDice(dice);
        return dice.getValue();
    }

    public void moveCurrentPiece(int value) {
        Player currentPlayer = getCurrentPlayer();
        currentPlayer.setPosition(adjustPosition(currentPlayer.getPosition(), value));
        notifyObservers();
        setChanged();
    }

    public void increseTurn(){
        turn++;
    }

    public Square getPlayerSquare(Player player){
        return board.getSquares()[player.getPosition()];
    }

    public Player[] getPlayers(){
        return players;
    }

}
