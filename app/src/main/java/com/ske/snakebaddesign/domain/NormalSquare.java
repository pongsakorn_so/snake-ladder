package com.ske.snakebaddesign.domain;

import android.graphics.Color;

import com.ske.snakebaddesign.activities.GameActivity;

/**
 * Created by Asus on 3/15/2016.
 */
public class NormalSquare extends Square {
    private final int squareColor = Color.parseColor("#87aa4c");
    private final int textColor = Color.parseColor("#cfe8a6");

    public NormalSquare(String label){
        super();
        setLabel(label);
        setSquareColor(squareColor);
        setTextColor(textColor);
    }

    public GameAction getGameAction(){
        GameAction doNothing = new GameAction() {
            @Override
            public void doAction(GameActivity gameActivity) {

            }
        };

        return doNothing;
    }
}
