package com.ske.snakebaddesign.domain;

import android.content.DialogInterface;
import android.graphics.Color;

import com.ske.snakebaddesign.activities.GameActivity;

/**
 * Created by Asus on 3/17/2016.
 */
public class GoalSquare extends Square {

    private final String label = "GOAL";
    private final int squareColor = Color.YELLOW;
    private final int textColor = Color.BLACK;

    public GoalSquare(){
        super();
        setLabel(label);
        setSquareColor(squareColor);
        setTextColor(textColor);
    }

    @Override
    public GameAction getGameAction() {

        GameAction winGame = new GameAction() {
            @Override
            public void doAction(final GameActivity gameActivity) {

                String title = "Game Over";
                String msg = gameActivity.getGame().getCurrentPlayer().getName() + "WIN";
                DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        gameActivity.resetGame();
                        dialog.dismiss();
                    }
                };
                gameActivity.displayDialog(title, msg, listener);

            }
        };

        return  winGame;
    }
}
