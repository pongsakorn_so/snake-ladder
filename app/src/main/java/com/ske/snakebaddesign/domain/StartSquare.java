package com.ske.snakebaddesign.domain;

import android.graphics.Color;

import com.ske.snakebaddesign.activities.GameActivity;

/**
 * Created by Asus on 3/17/2016.
 */
public class StartSquare extends Square {
    private final String label = "START";
    private final int squareColor = Color.GRAY;
    private final int textColor = Color.GREEN;

    public StartSquare(){
        super();
        setLabel(label);
        setSquareColor(squareColor);
        setTextColor(textColor);
    }

    @Override
    public GameAction getGameAction() {
        return new GameAction() {
            @Override
            public void doAction(GameActivity gameActivity) {

            }
        };
    }
}
