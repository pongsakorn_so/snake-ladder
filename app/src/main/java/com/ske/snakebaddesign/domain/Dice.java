package com.ske.snakebaddesign.domain;

import java.util.List;
import java.util.Random;

/**
 * Created by Asus on 3/15/2016.
 */
public class Dice {
    private int[] faces = {1,2,3,4,5,6};
    private int value;

    public Dice(){
        value = faces[0];
    }

    public Dice(int[] faces){
        this.faces = faces;
        value = faces[0];
    }

    public int[] getFaces(){
        return faces;
    }

    public int getValue(){
        return value;
    }

    public void setValue(int value){
        this.value = value;
    }

    public int getNumFaces(){
        return faces.length;
    }

    public void roll(){
        value = 1 + new Random().nextInt(faces.length);
    }


}
