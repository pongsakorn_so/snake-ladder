package com.ske.snakebaddesign.domain;

import android.graphics.Color;

/**
 * Created by Asus on 3/15/2016.
 */
public abstract class Square {

    private String label;
    private int squareColor;
    private int textColor;

    public  Square(){
        this.label = "";
        this.squareColor = Color.WHITE;
        this.textColor = Color.BLACK;
    }

//    public Square(String label){
//        this.label = label;
//        this.squareColor = Color.WHITE;
//        this.textColor = Color.BLACK;
//    }
//
//    public Square(String label ,int color){
//        this.label = label;
//        this.squareColor = color;
//        this.textColor = Color.BLACK;
//    }
//
//    public Square(String label ,int color , int textColor){
//        this.label = label;
//        this.squareColor = color;
//        this.textColor = textColor;
//    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setSquareColor(int squareColor) {
        this.squareColor = squareColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public String getLabel() {
        return label;
    }

    public int getSquareColor() {
        return squareColor;
    }

    public int getTextColor() {
        return textColor;
    }

    public abstract GameAction getGameAction();
}
