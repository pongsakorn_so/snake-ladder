package com.ske.snakebaddesign.domain;

import com.ske.snakebaddesign.activities.GameActivity;

/**
 * Created by Asus on 3/17/2016.
 */
public interface GameAction {
    void doAction(GameActivity gameActivity);
}
